@extends('admin.layouts.app')

@section('panel')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive--md table-responsive">
                    <table class="table table--light style--two">
                        <thead>
                            <tr>
                                <th scope="col">@lang('Sl')</th>
                                <th scope="col">@lang('Product Name')</th>
                                <th scope="col">@lang('Thumbnail')</th>
                                <th scope="col">@lang('Price')</th>
                                <th scope="col">@lang('Business Volume (BV)')</th>

                                <th scope="col">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($products as $key => $product)
                            <tr>
                                <td data-label="@lang('Sl')">{{$key+1}}</td>
                                <td data-label="@lang('Name')">{{ __($product->name) }}</td>
                                <td data-label="@lang('Price')">{{ getAmount($product->price) }} {{$general->cur_text}}
                                </td>
                                <td data-label="@lang('Bv')">{{ $product->bv }}</td>
                                <td data-label="@lang('Referral Commission')"> {{ getAmount($product->ref_com) }}
                                    {{$general->cur_text}}</td>


                                <td data-label="@lang('Action')">
                                    <button type="button" class="icon-btn edit" data-toggle="tooltip"
                                        data-id="{{ $product->id }}" data-name="{{ $product->name }}"
                                        data-status="{{ $product->status }}" data-bv="{{ $product->bv }}"
                                        data-price="{{ getAmount($product->price) }}"
                                        data-ref_com="{{ getAmount($product->ref_com) }}"
                                        data-tree_com="{{ getAmount($product->tree_com) }}" data-original-title="Edit">
                                        <i class="la la-pencil"></i>
                                    </button>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-muted text-center" colspan="100%">{{ __($empty_message) }}</td>
                            </tr>
                            @endforelse

                        </tbody>
                    </table><!-- table end -->
                </div>
            </div>
            <div class="card-footer py-4">
                {{ $products->links('admin.partials.paginate') }}
            </div>
        </div>
    </div>
</div>



@endsection



@push('breadcrumb-plugins')
<a href="#" class="btn btn-sm btn--success add-plan"><i class="fa fa-fw fa-plus"></i>@lang('Add
    New')</a>

@endpush

@push('script')
<script>
"use strict";
(function($) {
    $('.edit').on('click', function() {
        var modal = $('#edit-plan');
        modal.find('.name').val($(this).data('name'));
        modal.find('.price').val($(this).data('price'));
        modal.find('.bv').val($(this).data('bv'));
        modal.find('.ref_com').val($(this).data('ref_com'));
        modal.find('.tree_com').val($(this).data('tree_com'));
        modal.find('input[name=daily_ad_limit]').val($(this).data('daily_ad_limit'));

        if ($(this).data('status')) {
            modal.find('.toggle').removeClass('btn--danger off').addClass('btn--success');
            modal.find('input[name="status"]').prop('checked', true);

        } else {
            modal.find('.toggle').addClass('btn--danger off').removeClass('btn--success');
            modal.find('input[name="status"]').prop('checked', false);
        }

        modal.find('input[name=id]').val($(this).data('id'));
        modal.modal('show');
    });

    $('.add-plan').on('click', function() {
        var modal = $('#add-plan');
        modal.modal('show');
    });
})(jQuery);
</script>
@endpush