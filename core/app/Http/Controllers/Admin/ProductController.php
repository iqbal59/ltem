<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;


class ProductController extends Controller
{
    public function product()
    {
        $page_title = 'Products';
        $empty_message = 'No Product found';
        $products = Product::paginate(getPaginate());
        return view('admin.product.index', compact('page_title', 'products', 'empty_message'));
    }

}